package com.platform.connect4.controller;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.platform.connect4.PlatformInit;
import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import static com.jayway.restassured.RestAssured.when;

/**
 * Created by christopherloganathan on 10/07/2016.
 *
 * Integration test between two users's win moves by validating rows
 * with all the success and failure test coverage
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = PlatformInit.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class IntegrationRowsValidationTest {


    @Value("${local.server.port}")
    int port;
    static String platformId;
    String username="xxxxxx";
    char platformObj='x';
    int column;

    @Before
    public void initSession(){
        RestAssured.port = port;

        username = "chris001";
        platformObj = 'M';
        Response response = when().
                    get("/connect4/main/initSession/{username}/{platformObj}", username, platformObj).
                    then().assertThat().
                    statusCode(HttpStatus.SC_OK).
                    body("reason", Matchers.isEmptyOrNullString()).extract().response();

        response.prettyPrint();

        IntegrationRowsValidationTest.platformId = response.body().jsonPath().getList("userList.platformId").get(0).toString();


    }

    @Test()
    public void  playTest() {

        username = "chris002";
        platformObj = 'C';

        when().
                get("/connect4/platform/userJoin/{username}/{platformId}/{platformObj}", username, IntegrationRowsValidationTest.platformId,platformObj).
                then().assertThat().
                statusCode(HttpStatus.SC_OK).
                body("reason", Matchers.isEmptyOrNullString()).extract().response().body().prettyPrint();


        username = "chris001";
        column = 1;

        when().
                get("/connect4/platform/start/{username}/{platformId}/{columnNo}", username, IntegrationRowsValidationTest.platformId,column).
                then().assertThat().
                statusCode(HttpStatus.SC_OK).
                body("reason", Matchers.isEmptyOrNullString()).extract().response().body().prettyPrint();

        username = "chris002";
        column = 7;

        when().
                get("/connect4/platform/userNextTurn/{username}/{platformId}/{columnNo}", username, IntegrationRowsValidationTest.platformId,column).
                then().assertThat().
                statusCode(HttpStatus.SC_OK).
                body("reason",  Matchers.isEmptyOrNullString()).extract().response().body().prettyPrint();

        username = "chris001";
        column = 2;

        when().
                get("/connect4/platform/start/{username}/{platformId}/{columnNo}", username, IntegrationRowsValidationTest.platformId,column).
                then().assertThat().
                statusCode(HttpStatus.SC_OK).
                body("reason", Matchers.isEmptyOrNullString()).extract().response().body().prettyPrint();

        username = "chris002";
        column = 6;

        when().
                get("/connect4/platform/userNextTurn/{username}/{platformId}/{columnNo}", username, IntegrationRowsValidationTest.platformId,column).
                then().assertThat().
                statusCode(HttpStatus.SC_OK).
                body("reason",  Matchers.isEmptyOrNullString()).extract().response().body().prettyPrint();


        username = "chris001";
        column = 3;

        when().
                get("/connect4/platform/start/{username}/{platformId}/{columnNo}", username, IntegrationRowsValidationTest.platformId,column).
                then().assertThat().
                statusCode(HttpStatus.SC_OK).
                body("reason", Matchers.isEmptyOrNullString()).extract().response().body().prettyPrint();

        username = "chris002";
        column = 5;

        when().
                get("/connect4/platform/userNextTurn/{username}/{platformId}/{columnNo}", username, IntegrationRowsValidationTest.platformId,column).
                then().assertThat().
                statusCode(HttpStatus.SC_OK).
                body("reason",  Matchers.isEmptyOrNullString()).extract().response().body().prettyPrint();


        username = "chris001";
        column = 4;

        when().
                get("/connect4/platform/userNextTurn/{username}/{platformId}/{columnNo}", username, IntegrationRowsValidationTest.platformId,column).
                then().assertThat().
                statusCode(HttpStatus.SC_OK).
                body("reason", Matchers.isEmptyOrNullString()).extract().response().body().prettyPrint();

        username = "chris002";
        column = 1;

        when().
                get("/connect4/platform/userNextTurn/{username}/{platformId}/{columnNo}", username, IntegrationRowsValidationTest.platformId,column).
                then().assertThat().
                statusCode(HttpStatus.SC_BAD_REQUEST).
                body("reason",  Matchers.notNullValue()).extract().response().body().prettyPrint();

        username = "chris001";
        column = 2;

        when().
                get("/connect4/platform/userNextTurn/{username}/{platformId}/{columnNo}", username, IntegrationRowsValidationTest.platformId,column).
                then().assertThat().
                statusCode(HttpStatus.SC_BAD_REQUEST).
                body("reason",  Matchers.notNullValue()).extract().response().body().prettyPrint();


    }

}
