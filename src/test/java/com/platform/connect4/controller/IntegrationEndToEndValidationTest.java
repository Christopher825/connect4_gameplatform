package com.platform.connect4.controller;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.platform.connect4.PlatformInit;
import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import static com.jayway.restassured.RestAssured.when;

/**
 * Created by christopherloganathan on 10/07/2016.
 *
 * Integration test between two users's moves from start till
 * end of the platform with all the success and failure test coverage
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = PlatformInit.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class IntegrationEndToEndValidationTest {


    @Value("${local.server.port}")
    int port;
    static String platformId;
    String username="xxxxxx";
    char platformObj='x';
    int column;

    @Before
    public void initSession(){
            RestAssured.port = port;
            if(platformId==null) {
                username = "chris001";
                platformObj = 'M';
                Response response = when().
                        get("/connect4/main/initSession/{username}/{platformObj}", username, platformObj).
                        then().assertThat().
                        statusCode(HttpStatus.SC_OK).
                        body("reason", Matchers.isEmptyOrNullString()).extract().response();

                response.prettyPrint();

                IntegrationEndToEndValidationTest.platformId = response.body().jsonPath().getList("userList.platformId").get(0).toString();
            }

    }

    @Test()
    public void  playTest1() {

       when().
                get("/connect4/platform/start/{username}/{platformId}/{columnNo}", username, IntegrationEndToEndValidationTest.platformId,column).
                then().assertThat().
                statusCode(HttpStatus.SC_BAD_REQUEST).
                body("reason", Matchers.notNullValue()).extract().response().body().prettyPrint();

    }

   @Test()
    public void playTest2() {

        String platformId = IntegrationEndToEndValidationTest.platformId + username;
        when().
                get("/connect4/platform/userJoin/{username}/{platformId}/{platformObj}", username,platformId,platformObj).
                then().assertThat().
                statusCode(HttpStatus.SC_BAD_REQUEST).
                body("reason", Matchers.notNullValue()).extract().response().body().prettyPrint();

    }

   @Test()
    public void playTest3() {
       username = "chris001";
       platformObj = 'M';
        when().
                get("/connect4/platform/userJoin/{username}/{platformId}/{platformObj}", username, IntegrationEndToEndValidationTest.platformId,platformObj).
                then().assertThat().
                statusCode(HttpStatus.SC_BAD_REQUEST).
                body("reason", Matchers.notNullValue()).extract().response().body().prettyPrint();

    }

    @Test()
    public void playTest4() {

        username = "chris002";
        platformObj = 'M';

        when().
                get("/connect4/platform/userJoin/{username}/{platformId}/{platformObj}", username, IntegrationEndToEndValidationTest.platformId,platformObj).
                then().assertThat().
                statusCode(HttpStatus.SC_BAD_REQUEST).
                body("reason",  Matchers.notNullValue()).extract().response().body().prettyPrint();

    }

    @Test()
    public void  playTest5() {

       username = "chris002";
        platformObj = 'C';

        when().
                get("/connect4/platform/userJoin/{username}/{platformId}/{platformObj}", username, IntegrationEndToEndValidationTest.platformId,platformObj).
                then().assertThat().
                statusCode(HttpStatus.SC_OK).
                body("reason", Matchers.isEmptyOrNullString()).extract().response().body().prettyPrint();

    }

    @Test()
    public void  playTest6() {

        username = "chris002";

        when().
                get("/connect4/platform/start/{username}/{platformId}/{columnNo}", username, IntegrationEndToEndValidationTest.platformId,column).
                then().assertThat().
                statusCode(HttpStatus.SC_BAD_REQUEST).
                body("reason", Matchers.notNullValue()).extract().response().body().prettyPrint();

    }

    @Test()
    public void  playTest7() {

        username = "chris001";
        column = 2;

        when().
                get("/connect4/platform/start/{username}/{platformId}/{columnNo}", username, IntegrationEndToEndValidationTest.platformId,column).
                then().assertThat().
                statusCode(HttpStatus.SC_OK).
                body("reason", Matchers.isEmptyOrNullString()).extract().response().body().prettyPrint();

    }

    @Test()
    public void  playTest8() {

        when().
                get("/connect4/platform/userJoin/{username}/{platformId}/{platformObj}", username, IntegrationEndToEndValidationTest.platformId,platformObj).
                then().assertThat().
                statusCode(HttpStatus.SC_BAD_REQUEST).
                body("reason",  Matchers.notNullValue()).extract().response().body().prettyPrint();


    }

    @Test()
    public void  playTest9() {

        String platformId = IntegrationEndToEndValidationTest.platformId + username;

        when().
                get("/connect4/platform/userNextTurn/{username}/{platformId}/{columnNo}", username,platformId,column).
                then().assertThat().
                statusCode(HttpStatus.SC_BAD_REQUEST).
                body("reason",  Matchers.notNullValue()).extract().response().body().prettyPrint();

    }

    @Test()
    public void  playTest10() {

        when().
                get("/connect4/platform/userNextTurn/{username}/{platformId}/{columnNo}", username, IntegrationEndToEndValidationTest.platformId,column).
                then().assertThat().
                statusCode(HttpStatus.SC_BAD_REQUEST).
                body("reason",  Matchers.notNullValue()).extract().response().body().prettyPrint();

    }

    @Test()
    public void  playTest11() {

        username = "chris001";
        column = 4;

        when().
                get("/connect4/platform/userNextTurn/{username}/{platformId}/{columnNo}", username, IntegrationEndToEndValidationTest.platformId,column).
                then().assertThat().
                statusCode(HttpStatus.SC_BAD_REQUEST).
                body("reason",  Matchers.notNullValue()).extract().response().body().prettyPrint();

    }

    @Test()
    public void  playTest12() {

        String platformId = IntegrationEndToEndValidationTest.platformId + username;

        when().
                get("/connect4/platform/pause/{platformId}",platformId).
                then().assertThat().
                statusCode(HttpStatus.SC_BAD_REQUEST).
                body("reason",  Matchers.notNullValue()).extract().response().body().prettyPrint();

    }

    @Test()
    public void  playTest13() {

        when().
                get("/connect4/platform/pause/{platformId}", IntegrationEndToEndValidationTest.platformId).
                then().assertThat().
                statusCode(HttpStatus.SC_OK).
                body("reason",  Matchers.isEmptyOrNullString()).extract().response().body().prettyPrint();

    }

    @Test()
    public void  playTest14() {

        username = "chris002";
        column = 8;

        when().
                get("/connect4/platform/userNextTurn/{username}/{platformId}/{columnNo}", username, IntegrationEndToEndValidationTest.platformId,column).
                then().assertThat().
                statusCode(HttpStatus.SC_BAD_REQUEST).
                body("reason",  Matchers.notNullValue()).extract().response().body().prettyPrint();

    }

    @Test()
    public void  playTest15() {

        String platformId = IntegrationEndToEndValidationTest.platformId + username;

        when().
                get("/connect4/platform/resume/{platformId}",platformId).
                then().assertThat().
                statusCode(HttpStatus.SC_BAD_REQUEST).
                body("reason",  Matchers.notNullValue()).extract().response().body().prettyPrint();

    }

    @Test()
    public void  playTest16() {

        when().
                get("/connect4/platform/resume/{platformId}", IntegrationEndToEndValidationTest.platformId).
                then().assertThat().
                statusCode(HttpStatus.SC_OK).
                body("reason",  Matchers.isEmptyOrNullString()).extract().response().body().prettyPrint();

    }


    @Test()
    public void  playTest17() {

        username = "chris002";
        column = 8;

        when().
                get("/connect4/platform/userNextTurn/{username}/{platformId}/{columnNo}", username, IntegrationEndToEndValidationTest.platformId,column).
                then().assertThat().
                statusCode(HttpStatus.SC_BAD_REQUEST).
                body("reason",  Matchers.notNullValue()).extract().response().body().prettyPrint();

    }

    @Test()
    public void  playTest18() {


        username = "chris002";
        column = 4;

        when().
                get("/connect4/platform/userNextTurn/{username}/{platformId}/{columnNo}", username, IntegrationEndToEndValidationTest.platformId,column).
                then().assertThat().
                statusCode(HttpStatus.SC_OK).
                body("reason",  Matchers.isEmptyOrNullString()).extract().response().body().prettyPrint();

    }

    @Test()
    public void  playTest19() {

        String platformId = IntegrationEndToEndValidationTest.platformId + username;
        when().
                get("/connect4/platform/userQuit/{username}/{platformId}", username,platformId).
                then().assertThat().
                statusCode(HttpStatus.SC_BAD_REQUEST).
                body("reason",  Matchers.notNullValue()).extract().response().body().prettyPrint();

    }

    @Test()
    public void  playTest20() {

        when().
                get("/connect4/platform/userQuit/{username}/{platformId}", username, IntegrationEndToEndValidationTest.platformId).
                then().assertThat().
                statusCode(HttpStatus.SC_BAD_REQUEST).
                body("reason",  Matchers.notNullValue()).extract().response().body().prettyPrint();

    }

    @Test()
    public void  playTest21() {

        username = "chris001";

        when().
                get("/connect4/platform/userQuit/{username}/{platformId}", username, IntegrationEndToEndValidationTest.platformId).
                then().assertThat().
                statusCode(HttpStatus.SC_BAD_REQUEST).
                body("reason",  Matchers.notNullValue()).extract().response().body().prettyPrint();

    }

    @Test()
    public void  playTest22() {

        username = "chris002";

        when().
                get("/connect4/platform/exit/{username}/{platformId}", username, IntegrationEndToEndValidationTest.platformId).
                then().assertThat().
                statusCode(HttpStatus.SC_BAD_REQUEST).
                body("reason",  Matchers.notNullValue()).extract().response().body().prettyPrint();

    }

    @Test()
    public void  playTest23() {

        username = "chris002";

        when().
                get("/connect4/platform/userQuit/{username}/{platformId}", username, IntegrationEndToEndValidationTest.platformId).
                then().assertThat().
                statusCode(HttpStatus.SC_OK).
                body("reason",  Matchers.isEmptyOrNullString()).extract().response().body().prettyPrint();

    }

    @Test()
    public void  playTest24() {

        String platformId = IntegrationEndToEndValidationTest.platformId + username;
        when().
                get("/connect4/platform/exit/{username}/{platformId}", username,platformId).
                then().assertThat().
                statusCode(HttpStatus.SC_BAD_REQUEST).
                body("reason",  Matchers.notNullValue()).extract().response().body().prettyPrint();

    }

    @Test()
    public void  playTest25() {

        username = "chris001";

        when().
                get("/connect4/platform/exit/{username}/{platformId}", username, IntegrationEndToEndValidationTest.platformId).
                then().assertThat().
                statusCode(HttpStatus.SC_OK).
                body("reason",  Matchers.isEmptyOrNullString()).and().
                body("userList", Matchers.isEmptyOrNullString()).and().
                body("platform",  Matchers.isEmptyOrNullString()).
                extract().response().body().prettyPrint();

    }







}
