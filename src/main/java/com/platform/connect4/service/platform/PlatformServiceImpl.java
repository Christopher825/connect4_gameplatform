package com.platform.connect4.service.platform;

import com.platform.connect4.dao.responseObj.Platform;
import com.platform.connect4.dao.platform.PlatformDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by christopherloganathan on 10/07/2016.
 */
@Service
public class PlatformServiceImpl implements PlatformService{

    @Autowired
    private PlatformDao platformDao;


    @Override
    public void savePlatform(Platform platform) {

        platformDao.savePlatform(platform);
    }

    @Override
    public void removePlatform(String platformId) {
        platformDao.removePlatform(platformId);
    }

    @Override
    public Platform getPlatform(String platformId) {
        return platformDao.getPlatform(platformId);
    }


}
