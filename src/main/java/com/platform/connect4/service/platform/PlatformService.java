package com.platform.connect4.service.platform;


import com.platform.connect4.dao.platform.PlatformDao;

/**
 * Created by christopherloganathan on 10/07/2016.
 */
public interface PlatformService  extends PlatformDao {}
