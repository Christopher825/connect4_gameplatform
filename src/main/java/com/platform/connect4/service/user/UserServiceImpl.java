package com.platform.connect4.service.user;

import com.platform.connect4.dao.responseObj.User;
import com.platform.connect4.dao.user.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * Created by christopherloganathan on 10/07/2016.
 */
@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserDao userDao;


    @Override
    public void saveUser(User user) {
         userDao.saveUser(user);
    }

    @Override
    public void removeUser(String platformId,String username) {

        userDao.removeUser(platformId,username);
    }

    @Override
    public void removeAllUser(String platformId) {
        userDao.removeAllUser(platformId);
    }

    @Override
    public User getUser(String platformId, String username) {
        return userDao.getUser(platformId,username);
    }


    @Override
    public boolean isUserSelectedObject(String platformId, char platformObj) {
        return userDao.isUserSelectedObject(platformId,platformObj);
    }

    @Override
    public List<User> getAllUser(String platformId) {
        return userDao.getAllUser(platformId);
    }


}
