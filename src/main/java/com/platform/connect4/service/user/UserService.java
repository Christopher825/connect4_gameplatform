package com.platform.connect4.service.user;

import com.platform.connect4.dao.user.UserDao;

/**
 * Created by christopherloganathan on 10/07/2016.
 */
public interface UserService extends UserDao {}
