package com.platform.connect4.service.util;

import com.platform.connect4.dao.utilObj.UserStack;
import com.platform.connect4.dao.util.UserStackDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by christopherloganathan on 10/07/2016.
 */
@Service
public class UserStackServiceImpl implements UserStackService {

    @Autowired
    private UserStackDao userStackDao;


    @Override
    public void saveUserStack(UserStack userStack) {
        userStackDao.saveUserStack(userStack);
    }

    @Override
    public void removeUserStack(String platformId) {
        userStackDao.removeUserStack(platformId);
    }

    @Override
    public UserStack getUserStack(String platformId) {
        return userStackDao.getUserStack(platformId);
    }
}
