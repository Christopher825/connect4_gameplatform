package com.platform.connect4.service.util;

import com.platform.connect4.dao.util.UserStackDao;

/**
 * Created by christopherloganathan on 10/07/2016.
 */
public interface UserStackService extends UserStackDao {}
