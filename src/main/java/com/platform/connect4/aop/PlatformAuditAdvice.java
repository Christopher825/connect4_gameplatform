package com.platform.connect4.aop;

import com.platform.connect4.annotation.PlatformAudit;
import com.platform.connect4.utils.PlatformUtil;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import java.util.Map;

/**
 * Created by christopherloganathan on 10/07/2016.
 */
@Component
@Aspect
public class PlatformAuditAdvice {

    private static final Logger logger = LoggerFactory.getLogger(PlatformAuditAdvice.class);


    @Before("execution(* com.platform.connect4.controller.platformController.*Controller.*(..)) && @annotation(platformAudit)")
    public void auditPlatformSession(PlatformAudit platformAudit) {

        logger.debug(platformAudit.value());

    }

    @AfterReturning(
            pointcut = "execution(* com.platform.connect4.error.ErrorHandler.handler(..))",
            returning = "stringExceptionMap")
    public void logAfterReturningCaughtException(Map<String,Exception> stringExceptionMap){
        PlatformUtil.handleCaughtException(stringExceptionMap.entrySet().
                iterator().next().getKey(),stringExceptionMap.entrySet().
                iterator().next().getValue());

    }

    @AfterReturning(
            pointcut = "execution(* com.platform.connect4.error.ErrorHandler.handler(..))",
            returning = "message")
    public void logAfterReturningMessage(String message){
          logger.warn(message);
    }
}
