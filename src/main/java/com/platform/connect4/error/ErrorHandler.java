package com.platform.connect4.error;


import org.springframework.stereotype.Component;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by christopherloganathan on 10/07/2016.
 */

@Component
public class ErrorHandler {

    public Map<String,Exception> handler(String errorRef,Exception exception){
        Map<String,Exception> stringExceptionMap = new HashMap<>();
        stringExceptionMap.put(errorRef,exception);
        return stringExceptionMap;
    }

    public String handler(String message){

        return message;
    }
}
