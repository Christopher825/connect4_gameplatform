package com.platform.connect4.utils;

/**
 * Created by christopherloganathan on 10/07/2016.
 */
public final class CommandMode {

    public static final String CMD_USER_NEXT_TURN = "User next turn on the platform";
    public static final String CMD_START_PLATFORM = "The host start to initiate a move on the platform";
    public static final String CMD_PAUSE_PLATFORM = "Hold all moves by all user on the platform";
    public static final String CMD_RESUME_PLATFORM = "Resume all moves by all users on the platform";
    public static final String CMD_USER_JOIN_PLATFORM ="User joins the platform";
    public static final String CMD_USER_QUIT_PLATFORM = "User quits the platform";
    public static final String CMD_EXIT_PLATFORM = "Terminate the platform session by the host";
    public static final String CMD_INITIATE_PLATFORM_SESSION = "Initialising platform session with a " +
            "new host";

    private CommandMode() { }
}
