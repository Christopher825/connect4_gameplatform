package com.platform.connect4.utils;


import com.platform.connect4.dao.responseObj.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;


/**
 * Created by christopherloganathan on 10/07/2016.
 */
public class PlatformUtil {

    private static final Logger logger = LoggerFactory.getLogger(PlatformUtil.class);

    public static final void handleCaughtException(String errorRef,Exception exception) {

        logger.error("Error ref["+errorRef+"]",exception);
    }

    public static void displayPlatform(char[][] platform, String platformId, List<User> users, String platformState){
        System.out.println("Platform Id : " + platformId);
        System.out.println("Joined users|Selected object : " + getListOfUser(users));
        System.out.println("Platform state : " + platformState);
        System.out.println("_______________");
        System.out.println();
        for (int p = platform.length - 1; p >= 0; p--) {
            System.out.print("|");
            for (int t = 0; t < platform[p].length; t++)
                System.out.print(platform[p][t] != '\u0000'
                        ? platform[p][t] + "|" : " |");
            System.out.println();
        }
        System.out.println("_______________");

    }

    static  String getListOfUser(List<User> userList) {
        return userList.stream()
                .map(u -> String.format("(%s)", u.getUsername()
                        + (u.isHost() ? "[Host]" : "") + (u.isNextTurn() ?
                        "[Next Turn]" : "") + "|" + u.getPlatformObj()))
                .collect(Collectors.joining(", "));

    }

    public static String getNextOpponent(String currentOpponent,List<String> opponentList){
        for (ListIterator<String> it = opponentList.listIterator(); it.hasNext();) {
            String opponent = it.next();
            if(opponent.equals(currentOpponent))
                return it.hasNext() ?it.next():opponentList.get(0);
        }
        return currentOpponent;
    }




}
