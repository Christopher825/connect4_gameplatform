package com.platform.connect4.utils;

import java.util.*;



/**
 * Created by christopherloganathan on 10/07/2016.
 */
public class PlatformAI {

    public static Map<Boolean,char[][]> dropObject(int cl,char platformObj, char[][] platform) {
        Map<Boolean,char[][]> mapObject = new HashMap<>();
        try {
            for (int p = 0; p < platform.length; p++) {
                if (platform[p][cl-1] == '\u0000') {
                    platform[p][cl-1] = platformObj;
                    mapObject.put(Boolean.TRUE, platform);
                    return mapObject;
                }
            }
        }catch(ArrayIndexOutOfBoundsException ex){}
        mapObject.put(Boolean.FALSE,platform);
        return mapObject;
    }

    public static char[][] removeObject(char platformObj, char[][] platform) {
        for (int p = 0; p < platform.length; p++) {
            for (int t = 0; t < platform[p].length; t++) {
                if (platform[p][t] == platformObj)
                    platform[p][t] = '\u0000';
            }
        }
        return platform;
    }

    public static boolean isValidWin(char[][] platform) {

        return isValidConnectFour(platform);
    }

    public static boolean isValidDraw(char[][] platform) {
        for (int p = 0; p < platform.length; p++)
            for (int t = 0; t < platform[p].length; t++)
                if (platform[p][t] == '\u0000')
                    return false;

        return true;
    }

    static boolean isValidConnectFour(char[][] platform) {
        int cntRws = platform.length;
        int cntCls = platform[0].length;
        int cntDiag;
        char[] diag;

        // validate rows of the platform
        for (int r = 0; r < cntRws; r++) {
            if (isValidConnectFour(platform[r]))
                return true;
        }

        // validate columns of the platform
        for (int c = 0; c < cntCls; c++) {
            char[] cl = new char[cntRws];

            for (int r = 0; r < cntRws; r++)
                cl[r] = platform[r][c];

            if (isValidConnectFour(cl))
                return true;
        }

        // validate lower part of diagonal
        for (int r = 0; r < cntRws - 3; r++) {
            cntDiag = Math.min(cntRws - r,
                    cntCls);
            diag = new char[cntDiag];
            for (int d = 0; d < cntDiag; d++)
                diag[d] = platform[d + r][d];

            if (isValidConnectFour(diag))
                return true;
        }

        // validate upper part of diagonal
        for (int c = 1; c < cntCls - 3; c++) {
            cntDiag = Math.min(cntCls - c,
                    cntRws);
            diag = new char[cntDiag];
            for (int d = 0; d < cntDiag; d++)
                diag[d] = platform[d][d + c];

            if (isValidConnectFour(diag))
                return true;
        }

        // validate left part of sub-diagonal
        for (int c = 3; c < cntCls; c++) {
            cntDiag = Math.min(c + 1, cntRws);
            diag = new char[cntDiag];

            for (int d = 0; d < cntDiag; d++)
                diag[d] = platform[d][c - d];

            if (isValidConnectFour(diag))
                return true;
        }

        // validate right part of sub-diagonal
        for (int r = 1; r < cntRws - 3; r++) {
            cntDiag = Math.min(cntRws - r,
                    cntCls);
            diag= new char[cntDiag];

            for (int d = 0; d < cntDiag; d++)
                diag[d] = platform[d + r][cntCls - d - 1];

            if (isValidConnectFour(diag))
                return true;
        }

        return false;
    }

    static boolean isValidConnectFour(char[] platform) {
        for (int n = 0; n < platform.length - 3; n++) {
            boolean result = true;
            for (int m = n; m < n + 3; m++) {
                if (platform[m] == '\u0000' || platform[m] != platform[m + 1]) {
                    result = false;
                    break;
                }
            }
            if (result)
                return true;

        }

        return false;
    }

}
