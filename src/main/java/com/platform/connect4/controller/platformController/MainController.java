package com.platform.connect4.controller.platformController;


import com.platform.connect4.annotation.PlatformAudit;
import com.platform.connect4.controller.AbstractController;
import com.platform.connect4.dao.responseObj.Platform;
import com.platform.connect4.dao.responseObj.PlatformResponse;
import com.platform.connect4.dao.responseObj.User;
import com.platform.connect4.dao.utilObj.UserStack;
import com.platform.connect4.service.platform.PlatformService;
import com.platform.connect4.service.user.UserService;
import com.platform.connect4.service.util.UserStackService;
import com.platform.connect4.utils.CommandMode;
import com.platform.connect4.utils.PlatformUtil;
import org.jsondoc.core.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;

/**
 * Created by christopherloganathan on 10/07/2016.
 */

@Api(name = "connect-4 main controller", description = "Provides initial platform connect-4 session service by the host")
@ApiErrors(apierrors = {
        @ApiError(code = "400", description = "Invalid request to the server"),
        @ApiError(code = "500", description = "Internal server error")
})
@RestController
@RequestMapping("/main")
public class MainController extends AbstractController {



    @Autowired
    private PlatformService platformService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserStackService userStackService;


    @ApiMethod(description = CommandMode.CMD_INITIATE_PLATFORM_SESSION)
    @PlatformAudit(CommandMode.CMD_INITIATE_PLATFORM_SESSION)
    @RequestMapping("/initSession/{username}/{platformObj}")
    public @ApiResponseObject
    ResponseEntity<PlatformResponse> initSession(@ApiPathParam(name = "username", description =
            "user's username") @PathVariable String username, @ApiPathParam(name = "platformObj", description =
            "user's platform object")@PathVariable char platformObj) {

        try {

                Platform platform = new Platform();
                platform.setState(PlatformStateBean.WAITING.name());
                platformService.savePlatform(platform);
                User user = new User(username,platform.getRid().toString(),platformObj);
                user.setHost(true);
                user.setNextTurn(true);
                userService.saveUser(user);
                UserStack userStack = new UserStack();
                userStack.setPlatformId(platform.getRid().toString());
                userStack.setOpponentList(new ArrayList<>(Arrays.asList(user.getUsername())));
                userStackService.saveUserStack(userStack);
                List<User> userList = userService.getAllUser(platform.getRid().toString());
                displayPlatform(platform.getPlatform(), platform.getRid().toString(),
                        userList,platform.getState());//for better platform view at server console

               return response(platform,userList);


        }catch(Exception ex){

            return response("Internal server error with error ref["
                    +errorHandler(ex)+"]",HttpStatus.INTERNAL_SERVER_ERROR);

        }


    }

    protected  void displayPlatform(char[][] platform, String platformId,
                                    List<User> userList, String platformState){
        PlatformUtil.displayPlatform(platform,platformId,userList,platformState);

    }
    protected ResponseEntity<PlatformResponse> response(String reason,HttpStatus httpStatus){

        ResponseEntity<PlatformResponse> platformResponseResponseEntity;
        PlatformResponse platformResponse = new PlatformResponse();
        platformResponse.setReason(reason);
        platformResponseResponseEntity = new ResponseEntity<>(
                platformResponse,
                httpStatus
        );
        errorHandler(reason);
        return platformResponseResponseEntity;

    }

    protected ResponseEntity<PlatformResponse> response(Platform platform,List<User> userList){

        ResponseEntity<PlatformResponse> platformResponseResponseEntity;
        PlatformResponse platformResponse = new PlatformResponse();
        platformResponse.setPlatform(platform);
        platformResponse.setUserList(userList);
        platformResponseResponseEntity = new ResponseEntity<>(
                platformResponse,
                HttpStatus.OK
        );
        return platformResponseResponseEntity;
    }

}
