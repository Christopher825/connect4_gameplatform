package com.platform.connect4.controller.platformController;


import com.platform.connect4.annotation.PlatformAudit;
import com.platform.connect4.controller.AbstractController;
import com.platform.connect4.dao.responseObj.Platform;
import com.platform.connect4.dao.responseObj.PlatformResponse;
import com.platform.connect4.dao.responseObj.User;
import com.platform.connect4.dao.utilObj.UserStack;
import com.platform.connect4.service.platform.PlatformService;
import com.platform.connect4.service.user.UserService;
import com.platform.connect4.service.util.UserStackService;
import com.platform.connect4.utils.CommandMode;
import com.platform.connect4.utils.PlatformAI;
import com.platform.connect4.utils.PlatformUtil;
import org.jsondoc.core.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.Map;


/**
 * Created by christopherloganathan on 10/07/2016.
 */
@Api(name = "connect-4 platform controller", description = "Provides platform that handles connect-4 services to the users")
@ApiErrors(apierrors = {
        @ApiError(code = "400", description = "Invalid request to the server"),
        @ApiError(code = "500", description = "Internal server error")
})
@RestController
@RequestMapping("/platform")
public class PlatformController  extends AbstractController {


   @Autowired
   private PlatformService platformService;

   @Autowired
   private UserService userService;

   @Autowired
   private UserStackService userStackService;



   @ApiMethod(description = CommandMode.CMD_START_PLATFORM)
   @PlatformAudit(CommandMode.CMD_START_PLATFORM)
   @RequestMapping("/start/{username}/{platformId}/{columnNo}")
   public @ApiResponseObject
   ResponseEntity<PlatformResponse> start(
           @ApiPathParam(name = "platformId", description =
                   "user's platformId") @PathVariable String platformId,
           @ApiPathParam(name = "username", description =
                   "user's username") @PathVariable String username,
           @ApiPathParam(name = "columnNo", description =
                   "user's column number (1-7)") @PathVariable int columnNo){

      try{

         if(userService.getAllUser(platformId).size()>=2){

            Platform platform = platformService.getPlatform(platformId);
            User user = userService.getUser(platformId,username);
            List<User> userList;

            if(!user.isHost())
                return response("The user is not a host, therefore unable to start the platform",HttpStatus.BAD_REQUEST);
            else {

               platform.setPlatform(PlatformAI.dropObject(columnNo, user.getPlatformObj(), platform.getPlatform()).
                       entrySet().iterator().next().getValue());
               platform.setState(PlatformStateBean.ACTIVE.name());
               platformService.savePlatform(platform);
               user = userService.getUser(platformId, username);
               user.setNextTurn(false);
               userService.saveUser(user);
               String nextOpponent = PlatformUtil.getNextOpponent(username,
                       userStackService.getUserStack(platformId).getOpponentList());
               user = userService.getUser(platformId, nextOpponent);
               user.setNextTurn(true);
               userService.saveUser(user);
               userList = userService.getAllUser(platform.getRid().toString());
               displayPlatform(platform.getPlatform(), platform.getRid().toString(),
                       userList, platform.getState());//for better platform view at server console
               return response(platform,userList);
            }


         }else return  response("At least required 2 users in a platform",HttpStatus.BAD_REQUEST);


      }catch(Exception ex){
          return response("Internal server error with error ref["
                  +errorHandler(ex)+"]",HttpStatus.INTERNAL_SERVER_ERROR);
      }

   }

   @ApiMethod(description = CommandMode.CMD_USER_JOIN_PLATFORM)
   @PlatformAudit(CommandMode.CMD_USER_JOIN_PLATFORM)
   @RequestMapping("/userJoin/{username}/{platformId}/{platformObj}")
   public @ApiResponseObject
   ResponseEntity<PlatformResponse> userJoin(
           @ApiPathParam(name = "platformId", description =
                   "user's platformId") @PathVariable String platformId,
           @ApiPathParam(name = "username", description =
                   "user's username") @PathVariable String username,
           @ApiPathParam(name = "platformObj", description =
                   "user's platform object") @PathVariable char platformObj){

      try{

         Platform platform = platformService.getPlatform(platformId);
         User user = userService.getUser(platformId,username);
         UserStack userStack;
         List<User> userList;

         if(platform==null)

            return response("There is no existing platform session" +
                    " with platform Id : " + platformId,HttpStatus.BAD_REQUEST);

         else if(user!=null)

            return response("The user '"+username+ "' already exist in the platform", HttpStatus.BAD_REQUEST);

         else if(userService.isUserSelectedObject(platform.getRid().toString(),platformObj))

            return response("The platform object has already been used : "  + platformObj,HttpStatus.BAD_REQUEST);

         else if(platform.getState().equals(PlatformStateBean.ACTIVE.name()))

            return response("The user '"+username+"'is not allowed to join the " +
                            "platform as it is currently being active",
                    HttpStatus.BAD_REQUEST);

         else {

            user = new User(username,platform.getRid().toString(),platformObj);
            userService.saveUser(user);
            userStack = userStackService.getUserStack(platform.getRid().toString());
            userStack.getOpponentList().add(user.getUsername());
            userStackService.saveUserStack(userStack);
            userList = userService.getAllUser(platform.getRid().toString());
            displayPlatform(platform.getPlatform(), platform.getRid().toString(),
                    userList,platform.getState());//for better platform view at server console

            return response(platform,userList);

         }

      }catch(Exception ex){

         return response("Internal server error with error ref["
                 +errorHandler(ex)+"]",HttpStatus.INTERNAL_SERVER_ERROR);
      }


   }

   @ApiMethod(description = CommandMode.CMD_USER_NEXT_TURN)
   @PlatformAudit(CommandMode.CMD_USER_NEXT_TURN)
   @RequestMapping("/userNextTurn/{username}/{platformId}/{columnNo}")
   public @ApiResponseObject
   ResponseEntity<PlatformResponse> userNextTurn(
           @ApiPathParam(name = "platformId", description =
                   "user's platformId")@PathVariable String platformId,
           @ApiPathParam(name = "username", description =
                   "user's username") @PathVariable String username,
           @ApiPathParam(name = "columnNo", description =
                   "user's column number (1-7)") @PathVariable int columnNo){


      try{

         Platform platform = platformService.getPlatform(platformId);
         User user = userService.getUser(platformId,username);
         Map<Boolean,char[][]> mapDropObject;
         List<User> userList;

         if(platform==null)

            return response("There is no existing platform session" +
                    " with platform Id : " + platformId,HttpStatus.BAD_REQUEST);

         else if(user==null)

            return response("The user '" + username + "' doest't exist in the platform",
                    HttpStatus.BAD_REQUEST);

         else if(!user.isNextTurn())

            return response("It's not the user '"+username+"' turn currently",HttpStatus.BAD_REQUEST);

         else if(platform.getState().equals(PlatformStateBean.WAITING.name()))


            return response("The user '" + username + "'is not allowed to make a move on the " +
                    "platform as it is currently being paused", HttpStatus.BAD_REQUEST);

         else{

            mapDropObject= PlatformAI.dropObject(columnNo,user.getPlatformObj(),platform.getPlatform());
            if(!mapDropObject.entrySet().iterator().next().getKey())

                return response("The is no column available at the moment. " +
                       "Please try a another column",HttpStatus.BAD_REQUEST);
            else {

               platform.setPlatform(mapDropObject.entrySet().iterator().next().getValue());
               platformService.savePlatform(platform);
               user = userService.getUser(platform.getRid().toString(), username);

               if (PlatformAI.isValidWin(platform.getPlatform())) {

                  user.setWin(true);
                  user.setNextTurn(false);
                  userService.saveUser(user);

               } else if (PlatformAI.isValidDraw(platform.getPlatform())){

                  user.setDraw(true);
                  user.setNextTurn(false);
                  userService.saveUser(user);

               }else{

                  user.setNextTurn(false);
                  userService.saveUser(user);
                  String nextOpponent = PlatformUtil.getNextOpponent(user.getUsername(),
                          userStackService.getUserStack(platformId).getOpponentList());
                  user = userService.getUser(platformId,nextOpponent);
                  user.setNextTurn(true);
                  userService.saveUser(user);

               }

               userList = userService.getAllUser(platform.getRid().toString());
               displayPlatform(platform.getPlatform(), platform.getRid().toString(),
                       userList,platform.getState());//for better platform view at server console

               return response(platform,userList);
            }

         }

      }catch (Exception ex){

         return response("Internal server error with error ref["
                 +errorHandler(ex)+"]",HttpStatus.INTERNAL_SERVER_ERROR);
      }

   }

   @ApiMethod(description = CommandMode.CMD_PAUSE_PLATFORM)
   @PlatformAudit(CommandMode.CMD_PAUSE_PLATFORM)
   @RequestMapping("/pause/{platformId}")
   public @ApiResponseObject
   ResponseEntity<PlatformResponse> pause(
           @ApiPathParam(name = "platformId", description =
                   "user's platformId") @PathVariable String platformId){

      try{
         Platform platform = platformService.getPlatform(platformId);
         List<User> userList;

         if(platform==null)

            return response("There is no existing platform session with platform Id : " + platformId,
                    HttpStatus.BAD_REQUEST);
         else {

            platform.setState(PlatformStateBean.WAITING.name());
            platformService.savePlatform(platform);
            userList = userService.getAllUser(platform.getRid().toString());

            displayPlatform(platform.getPlatform(), platform.getRid().toString(),
                    userList, platform.getState());//for better platform view at server console

            return response(platform,userList);
         }

      }catch (Exception ex){

         return response("Internal server error with error ref["
                 +errorHandler(ex)+"]",HttpStatus.INTERNAL_SERVER_ERROR);
      }


   }

   @ApiMethod(description = CommandMode.CMD_RESUME_PLATFORM)
   @PlatformAudit(CommandMode.CMD_RESUME_PLATFORM)
   @RequestMapping("/resume/{platformId}")
   public @ApiResponseObject
   ResponseEntity<PlatformResponse> resume(
           @ApiPathParam(name = "platformId", description =
                   "user's platformId") @PathVariable String platformId){

      try{

         Platform platform = platformService.getPlatform(platformId);
         List<User> userList;

         if(platform==null)

             return response("There is no existing platform session" +
                    " with platform Id : " + platformId, HttpStatus.BAD_REQUEST);
         else {

            platform.setState(PlatformStateBean.ACTIVE.name());
            platformService.savePlatform(platform);
            userList = userService.getAllUser(platform.getRid().toString());

            displayPlatform(platform.getPlatform(), platform.getRid().toString(),
                    userList, platform.getState());//for better platform view at server console

            return response(platform,userList);
         }

      }catch (Exception ex){

         return response("Internal server error with error ref["
                 +errorHandler(ex)+"]",HttpStatus.INTERNAL_SERVER_ERROR);
      }

   }

   @ApiMethod(description = CommandMode.CMD_USER_QUIT_PLATFORM)
   @PlatformAudit(CommandMode.CMD_USER_QUIT_PLATFORM)
   @RequestMapping("/userQuit/{username}/{platformId}")
   public @ApiResponseObject
   ResponseEntity<PlatformResponse> userQuit(
           @ApiPathParam(name = "platformId", description =
                   "user's platformId")@PathVariable String platformId,
           @ApiPathParam(name = "username", description =
                   "user's username") @PathVariable String username){

      try{

         Platform platform = platformService.getPlatform(platformId);
         User user = userService.getUser(platformId,username);
         UserStack userStack;
         List<User> userList;

         if(platform==null)

            return response("There is no existing platform session with platform Id : " + platformId
            ,HttpStatus.BAD_REQUEST);

         else if(user==null)

            return response("The user '" + username + "' doest't exist in the platform",
                    HttpStatus.BAD_REQUEST);

         else if(user.isHost())

            return response("The user '" + username + "' has rights to end the platform only and " +
                    "not to quit from the platform",HttpStatus.BAD_REQUEST);

         else{

            userService.removeUser(platform.getRid().toString(),username);
            userStack = userStackService.getUserStack(platform.getRid().toString());
            userStack.getOpponentList().remove(username);
            userStackService.saveUserStack(userStack);
            platform.setPlatform(PlatformAI.removeObject(user.getPlatformObj(),platform.getPlatform()));
            platformService.savePlatform(platform);
            userList = userService.getAllUser(platform.getRid().toString());
            displayPlatform(platform.getPlatform(), platform.getRid().toString(),
                    userList, platform.getState());//for better platform view at server console

            return response(platform,userList);
         }

      }catch (Exception ex){

         return response("Internal server error with error ref[" +errorHandler(ex)+"]",
                 HttpStatus.INTERNAL_SERVER_ERROR);
      }

   }


   @ApiMethod(description = CommandMode.CMD_EXIT_PLATFORM)
   @PlatformAudit(CommandMode.CMD_EXIT_PLATFORM)
   @RequestMapping("/exit/{username}/{platformId}")
   public @ApiResponseObject
   ResponseEntity<PlatformResponse> exit(
           @ApiPathParam(name = "platformId", description =
                   "user's platformId")@PathVariable String platformId,
           @ApiPathParam(name = "username", description =
                   "user's username") @PathVariable String username){

      try{

         Platform platform = platformService.getPlatform(platformId);
         User user = userService.getUser(platformId,username);

         if(platform==null)

            return response("There is no existing platform session with platform Id : "
                    + platformId,HttpStatus.BAD_REQUEST);

         else if(!user.isHost())

            return response("The user '" + username + "' has no rights to end the platform and " +
                    "only able to quit from the platform",HttpStatus.BAD_REQUEST);

         else{

            userService.removeAllUser(platform.getRid().toString());
            userStackService.removeUserStack(platform.getRid().toString());
            platformService.removePlatform(platform.getRid().toString());
            platform = null;
            return response(platform,null);
         }

      }catch (Exception ex){

          return response("Internal server error with error ref[" +errorHandler(ex)+"]",
                  HttpStatus.INTERNAL_SERVER_ERROR);
      }

   }

   @Override
   protected  void displayPlatform(char[][] platform, String platformId,
                                   List<User> userList, String platformState){
      PlatformUtil.displayPlatform(platform,platformId,userList,platformState);

   }

   @Override
   protected ResponseEntity<PlatformResponse> response(String reason,HttpStatus httpStatus){

      ResponseEntity<PlatformResponse> platformResponseResponseEntity;
      PlatformResponse platformResponse = new PlatformResponse();
      platformResponse.setReason(reason);
      platformResponseResponseEntity = new ResponseEntity<>(
              platformResponse,
              httpStatus
      );
      errorHandler(reason);
      return platformResponseResponseEntity;

   }

   @Override
   protected ResponseEntity<PlatformResponse> response(Platform platform,List<User> userList){

      ResponseEntity<PlatformResponse> platformResponseResponseEntity;
      PlatformResponse platformResponse = new PlatformResponse();
      platformResponse.setPlatform(platform);
      platformResponse.setUserList(userList);
      platformResponseResponseEntity = new ResponseEntity<>(
              platformResponse,
              HttpStatus.OK
      );
      return platformResponseResponseEntity;
   }
}
