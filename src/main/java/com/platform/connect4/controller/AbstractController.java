package com.platform.connect4.controller;


import com.platform.connect4.dao.responseObj.Platform;
import com.platform.connect4.dao.responseObj.PlatformResponse;
import com.platform.connect4.dao.responseObj.User;
import com.platform.connect4.error.ErrorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.List;

/**
 * Created by christopherloganathan on 10/07/2016.
 */

public abstract class AbstractController {


    @Autowired
    private ErrorHandler errorHandler;


    protected enum PlatformStateBean {
        ACTIVE,
        WAITING
    }

    protected String errorHandler(Exception exception) {
        String errorRef = Long.toHexString(Double.doubleToLongBits(Math.random()));
        errorHandler.handler(errorRef,exception);
        return errorRef;
    }

    protected void errorHandler(String message) {

        errorHandler.handler(message);
    }

    protected  abstract void displayPlatform(char[][] platform, String platformId,
                                    List<User> userList, String platformState);


    protected abstract ResponseEntity<PlatformResponse> response(String reason,HttpStatus httpStatus);

    protected abstract ResponseEntity<PlatformResponse> response(Platform platform, List<User> userList);
}
