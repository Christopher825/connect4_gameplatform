package com.platform.connect4;


import org.jsondoc.spring.boot.starter.EnableJSONDoc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;


/**
 * Created by christopherloganathan on 10/07/2016.
 */
@SpringBootApplication
@EnableJSONDoc
public class PlatformInit extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(PlatformInit.class);
    }

    public static void main(String... args) {

        SpringApplication.run(PlatformInit.class, args);

    }
}
