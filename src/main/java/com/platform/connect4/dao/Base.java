package com.platform.connect4.dao;

import lombok.Getter;
import lombok.Setter;
import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;
import java.io.Serializable;
import java.util.UUID;

/**
 * Created by christopherloganathan on 10/07/2016.
 */
@ApiObject
public abstract class Base implements Serializable {


    private static final long serialVersionUID = -3757379148066343102L;
    @ApiObjectField(description = "generates random uuid")
    @Getter
    @Setter
    protected UUID rid = UUID.randomUUID();


}
