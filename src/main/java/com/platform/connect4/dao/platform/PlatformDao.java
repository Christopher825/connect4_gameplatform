package com.platform.connect4.dao.platform;


import com.platform.connect4.dao.responseObj.Platform;

/**
 * Created by christopherloganathan on 10/07/2016.
 */
public interface PlatformDao {

    void savePlatform(Platform platform);

    void removePlatform(String platformId);

    Platform getPlatform(String platformId);

}
