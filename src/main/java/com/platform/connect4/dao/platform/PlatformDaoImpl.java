package com.platform.connect4.dao.platform;

import com.platform.connect4.dao.AbstractDao;
import com.platform.connect4.dao.responseObj.Platform;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.stereotype.Repository;


/**
 * Created by christopherloganathan on 10/07/2016.
 */
@Repository
public class PlatformDaoImpl extends AbstractDao implements PlatformDao{


    @Override
    public void savePlatform(Platform platform) {
        HashOperations<String,String, Platform> ops = redisTemplate.opsForHash();
        ops.put(PrefixBean.PlatFormHashes.getPrefix(),platform.getRid().toString(), platform);
    }

    @Override
    public void removePlatform(String platformId) {
        HashOperations<String,String, Platform> ops = redisTemplate.opsForHash();
        ops.delete(PrefixBean.PlatFormHashes.getPrefix(),platformId);

    }

    @Override
    public Platform getPlatform(String platformId) {

        HashOperations<String,String, Platform> ops = redisTemplate.opsForHash();
        return ops.get(PrefixBean.PlatFormHashes.getPrefix(),platformId);
    }

}
