package com.platform.connect4.dao.util;


import com.platform.connect4.dao.utilObj.UserStack;

/**
 * Created by christopherloganathan on 10/07/2016.
 */

public interface UserStackDao {

    void saveUserStack(UserStack userStack);

    void removeUserStack(String platformId);

    UserStack getUserStack(String platformId);
}
