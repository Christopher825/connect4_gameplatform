package com.platform.connect4.dao.util;

import com.platform.connect4.dao.AbstractDao;
import com.platform.connect4.dao.utilObj.UserStack;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.stereotype.Repository;

/**
 * Created by christopherloganathan on 10/07/2016.
 */
@Repository
public class UserStackDaoImpl extends AbstractDao implements UserStackDao {


    @Override
    public void saveUserStack(UserStack userStack) {
        HashOperations<String,String, UserStack> ops = redisTemplate.opsForHash();
        ops.put(PrefixBean.OpponentHashes.getPrefix(),userStack.getPlatformId(), userStack);
    }

    @Override
    public void removeUserStack(String platformId) {
        HashOperations<String,String, UserStack> ops = redisTemplate.opsForHash();
        ops.delete(PrefixBean.OpponentHashes.getPrefix(),platformId);
    }

    @Override
    public UserStack getUserStack(String platformId){
        HashOperations<String,String, UserStack> ops = redisTemplate.opsForHash();
        return ops.get(PrefixBean.OpponentHashes.getPrefix(),platformId);
    }

}
