package com.platform.connect4.dao;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * Created by christopherloganathan on 10/07/2016.
 */
public abstract class AbstractDao {

    @Autowired
    protected RedisTemplate redisTemplate;

    protected enum PrefixBean {

        PlatFormHashes("#platform#"),
        OpponentHashes("#opponent#");

        @Getter
        String prefix;

        PrefixBean(String prefix) {
            this.prefix = prefix;
        }
    }
}
