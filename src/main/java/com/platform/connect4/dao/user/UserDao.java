package com.platform.connect4.dao.user;

import com.platform.connect4.dao.responseObj.User;

import java.util.List;


/**
 * Created by christopherloganathan on 10/07/2016.
 */
public interface UserDao {

    void saveUser(User user);

    void removeUser(String platformId,String username);

    void removeAllUser(String platformId);

    User getUser(String platformId,String username);

    boolean isUserSelectedObject(String platformId,char platformObj);

    List<User> getAllUser(String platformId);

}
