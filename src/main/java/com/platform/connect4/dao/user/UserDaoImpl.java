package com.platform.connect4.dao.user;


import com.platform.connect4.dao.AbstractDao;
import com.platform.connect4.dao.responseObj.User;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.stereotype.Repository;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by christopherloganathan on 10/07/2016.
 */
@Repository
public class UserDaoImpl extends AbstractDao implements UserDao{


    @Override
    public void saveUser(User user){
        HashOperations<String,String, User> ops = redisTemplate.opsForHash();
        ops.put(user.getPlatformId(),user.getUsername(), user);
    }

    @Override
    public void removeUser(String platformId,String username) {
        HashOperations<String,String, User> ops = redisTemplate.opsForHash();
        ops.delete(platformId,username);
    }

    @Override
    public void removeAllUser(String platformId){
        HashOperations<String,String, User> ops = redisTemplate.opsForHash();
        ops.delete(platformId,getAllUsernames(platformId));
    }


    @Override
    public User getUser(String platformId,String username){
        HashOperations<String,String, User> ops = redisTemplate.opsForHash();
        Map.Entry<String,User> userMap =  ops.entries(platformId)
                .entrySet()
                .stream()
                .filter(u ->
                   u.getValue().getPlatformId().equals(platformId)
                           && u.getValue().getUsername().equals(username)
                ).findFirst().orElse(null);

        return userMap==null?null:userMap.getValue();

    }


    @Override
    public boolean isUserSelectedObject(String platformId,char platformObj) {
        HashOperations<String,String, User> ops = redisTemplate.opsForHash();
        return ops.entries(platformId).entrySet()
                .stream()
                .filter(u ->
                   u.getValue().getPlatformId().equals(platformId) &&
                           u.getValue().getPlatformObj()==platformObj
                ).findAny().isPresent();

    }

    @Override
    public List<User> getAllUser(String platformId) {

        HashOperations<String,String, User> ops = redisTemplate.opsForHash();
        return ops.entries(platformId).entrySet()
                .stream()
                .filter(u -> u.getValue().getPlatformId().equals(platformId))
                .map(m -> m.getValue())
                .collect(Collectors.toList());
    }


    private Object[] getAllUsernames(String platformId) {

        HashOperations<String,String, User> ops = redisTemplate.opsForHash();
        return ops.entries(platformId).entrySet()
                .stream()
                .filter(u -> u.getValue().getPlatformId().equals(platformId))
                .map(m -> m.getValue().getUsername())
                .toArray();
    }
}
