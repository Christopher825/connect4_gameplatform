package com.platform.connect4.dao.responseObj;


import lombok.Getter;
import lombok.Setter;
import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;
import java.io.Serializable;

/**
 * Created by christopherloganathan on 10/07/2016.
 */

@ApiObject
public final class User implements Serializable {

    private static final long serialVersionUID = -6342773492101309L;

    @ApiObjectField(description = "user's username")
    @Getter
    private String username;
    @ApiObjectField(description = "to validate user's next turn" +
            " status for a move on platform object")
    @Getter @Setter
    private boolean isNextTurn;
    @ApiObjectField(description = "to validate user's win status")
    @Getter @Setter
    private boolean isWin;
    @ApiObjectField(description = "to validate user's draw status")
    @Getter @Setter
    private boolean isDraw;
    @ApiObjectField(description = "to indicate user is a host")
    @Getter @Setter
    private boolean isHost;
    @ApiObjectField(description = "user's platform id")
    @Getter
    private String platformId;
    @ApiObjectField(description = "user's platform object")
    @Getter
    private char platformObj;

    public User(String username,String platformId,char platformObj) {

        this.username = username;
        this.platformId = platformId;
        this.platformObj = platformObj;
    }
}
