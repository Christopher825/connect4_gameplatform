package com.platform.connect4.dao.responseObj;

import com.platform.connect4.dao.Base;
import lombok.Getter;
import lombok.Setter;
import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;

/**
 * Created by christopherloganathan on 10/07/2016.
 */
@ApiObject
public final class Platform extends Base {

    @ApiObjectField(description = "platform that holds objects and allow users to move")
    @Getter @Setter  private char[][] platform;
    @ApiObjectField(description = "indicating platform state")
    @Getter @Setter  private String state;

    public Platform() {

        platform = new char[6][7];
    }


}
