package com.platform.connect4.dao.responseObj;

import lombok.Getter;
import lombok.Setter;
import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;
import java.util.List;

/**
 * Created by christopherloganathan on 10/07/2016.
 */
@ApiObject
public final class PlatformResponse {

    @ApiObjectField(description = "collection of users per platform")
    @Getter @Setter private List<User> userList;
    @ApiObjectField(description = "collection of users's platform objects and its state")
    @Getter @Setter private Platform platform;
    @ApiObjectField(description = "reason generation from the server returning http status code other than 200")
    @Getter @Setter private String reason;

}
