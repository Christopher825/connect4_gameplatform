package com.platform.connect4.dao.utilObj;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.util.List;


/**
 * Created by christopherloganathan on 10/07/2016.
 */
public final class UserStack  implements Serializable {

    private static final long serialVersionUID = 5656104343100388099L;
    @Getter @Setter  private String platformId;
    @Getter @Setter  private List<String> opponentList;

}
