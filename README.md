# README #

Connect-4 game platform APIs

### Development technologies ###

* JDK 1.8
* Spring Boot v1.3.5.RELEASE
* Spring v4.2.6.RELEASE
* Spring Boot AOP
* Spring Boot Test
* Spring Boot Jetty Server
* Embedded Redis Server 0.6
* Spring Boot with Spring Data Redis
* Jacoco 0.7.5.201505241946 - Test coverage maven plugin
* Apache Maven 3.3.9 
* Jsondoc 1.2.15 - Self documenting library
* Rest-assured 2.9.0 - REST APIs integration testing

###Integration REST APIs testing###

* To test all : mvn clean test
* To test particular test script : mvn clean test -Dtest=<test_script>

###Integration REST APIs testing with test coverage plugin###

* To test all : mvn clean jacoco:prepare-agent install jacoco:report 
* To test particular test script  : mvn clean jacoco:prepare-agent install jacoco:report -Dtest=            <test_script>
* Report generation path : ${project.basedir}/target/site/jacoco/index.html

###Build & run###

* To build(skip test) : mvn clean package -Dmaven.test.skip=true
* To run : mvn spring-boot:run
* To build with main manifest attribute(skip test) : mvn clean package spring-boot:repackage -Dmaven.test.skip=true
* to run with main manifest attribute(skip test) : java -jar ${project.basedir}/target/connect4.war

###Jsondoc - Self documenting library###

* Base path : http://localhost:8080/connect4
* UI path : http://localhost:8080/connect4/jsondoc-ui.html
* Document path :http://localhost:8080/connect4/jsondoc

### Who do I talk to? ###

* Admin : Christopher Loganathan
* Email Contact : christopherloganathan@gmail.com